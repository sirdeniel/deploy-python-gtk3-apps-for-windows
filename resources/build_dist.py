# MIT License
#
# Copyright (c) 2019 David Daniel Lara Pazmiño
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import csv, shutil
from os import path, makedirs

def filter_python3_logs(opencsvfile, savepylogsfile, savepydirsfile, python3w = True):
    """
    Takes a csv log file from Procmon.exe and creates two files
    - savepylogsfile contains just python3.exe logs
    - savepydirsfile contains directory&files accessed by python3.exe
    Note: Double quote in each field is ommited, not harmful 
        for our purposes though
    """
    process_to_look = "python3w.exe" if python3w else "python3.exe"

    # prepare log list of python3.exe process
    python3_log_list = list()
    # prepare dirs_set for unique dirs accesed by python3.exe process
    dirs_set = set()

    # open csv file created by Procmon.exe
    with open(opencsvfile, newline='\n') as csvfile:
        file = csv.reader(csvfile, delimiter=',')
        for row in file:
            # row starts from 0 index element
            # position 1 has the process name
            # postition 4 has dirs accesed (even REGEDIT paths!)
            if row[1] == process_to_look and 'msys' in row[4]:
                python3_log_list.append(','.join(row))
                dirs_set.add(row[4])

    # save logs of filtered python3.exe registers
    with open(savepylogsfile, 'w') as f:
        for item in python3_log_list:
            f.write("%s\n" % item)
    
    # write these dirs_set accessed by python3.exe
    # into a file. Note: omits REGEDIT paths (yep, they are here)
    with open(savepydirsfile, 'w') as f:
        for item in sorted(dirs_set):
            if 'HKLM' not in item:
                f.write("%s\n" % item)

def make_distfolder(dirs_file, dst_path):
    """
    Based on dirs_file made by filter_python3_logs, copy files
    with their folder structure into dst_path
    Example:
    source: C:\\msys64\\mingw32\\bin\\python3.7.exe
    dst_path: C:\\msys64\\home\\USER\\dist
    action: Copy/create file into 
    C:\\msys64\\home\\USER\\dist\\msys64\\mingw32\\bin\\python3.exe
    """
    # if dst_path does not exists, create it
    if not path.isdir(dst_path):
        makedirs(dst_path)

    # destination full path
    dst = path.realpath(dst_path)

    # read dirs_file
    with open(dirs_file, 'r') as f:
        for line in f:
            # strip newline character
            srcfile = line.strip()
            # check if it's a file and it exists
            if path.isfile(srcfile):
                # get destination path structure
                src_folder = path.dirname(srcfile).strip("C:\\")
                dst_dir = path.join(dst, src_folder)
                print(dst_dir)
                # if directory doesn't exists, create it
                if not path.isdir(dst_dir):
                    makedirs(dst_dir)
                # copy file with all possible metadata
                shutil.copy2(srcfile, dst_dir)
        print('Done!')

if __name__ == "__main__":
    filter_python3_logs('Logfile.CSV','pylogs.txt', 'dirs_set.txt')
    make_distfolder('dirs_set.txt', 'dist')
